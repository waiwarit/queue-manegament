const express = require('express');
const app = express();
const port = 8080;
require('./db/Connection')
const serviceRoute = require('./route/Service')
const cors = require('cors')

app.use(cors());         //using CORS
app.use(express.json()); //using Json
app.use(serviceRoute);

/* Start Application */
app.listen(port, function(){
    console.log("App Start in Port " + port);
})