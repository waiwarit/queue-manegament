const mongoose = require('mongoose');
const moment = require('moment');
const request = require('request');

var QueueSchema = new mongoose.Schema({
    queue_name:{
        type:String,
        required:true,
    },
    queue_date:{
        type:String,
        required:true,
        validate(value){
            if(!moment(value,"YYYY-MM-DD LT", true).isValid()){
                throw new Error('Invalid started! ex. 2014-12-13 12:34 PM')
            }
        }
    },
    queue_about:{
        type:String,
        required:true
    },
    queue_create:{
        type:String,
        required:true,
        validate(value){
            validate_create(value)
        }
    },
    queue_priority:{
        type:Number,
        required:true,
        
    },
    queue_score:{
        type:Number,
        required:false
    },
    queue_position:{
        type:Number,
        required:false
    }
    
});

function validate_create(create_by){
    var role = ["Excutive", "Customer", "Insider", "Outsider"]
    if(!role.includes(create_by) ){
        throw new Error("Must create by Excutive, Customer, Insider, Outsider")
    };
}

module.exports = mongoose.model('Queue', QueueSchema);