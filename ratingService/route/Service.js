const express = require('express');
const myQueue = require('../db/Queue.model')
const router = express.Router();
const moment = require('moment');

router.get('/', (req, res) => {
    res.send("Welcome to QueueSystem");
})

router.post('/sentwork', async (req, res) => {
    try {
        const payload = req.body;
        validateForm(payload.queue_name, payload.queue_date, payload.queue_about, payload.queue_create, payload.queue_priority);
        const queue = new myQueue(payload)

        let score = sumScore(queue.queue_create, queue.queue_date, queue.queue_priority);
        queue.queue_score = score;
        queue.queue_priority = checkPriority(score);
        queue.queue_position = 0;
        await queue.save()
        // arr = positioning(score);
        // for (i = 0; i < arr.length; i++) {

        //     if (arr[i] == myQueue.find({}, { queue_score: arr[i] })) {
        //         console.log(myQueue.find({}, { queue_score: arr[i] }));
        //         var update = myQueue.findById(myQueue.find({ queue_score: arr[i] }).id);
        //         update.queue_position = i + 1;
        //         update.save()
        //     }
        // }

        const new_queue = await myQueue.find({ queue_score: 148 });
        console.log(new_queue);
        res.status(201).send({ message: payload })
    } catch (e) {
        res.status(400).send({ message: e.message });
    }
});

router.get('/queue', async (req, res) => {
    try {
        const queue = await myQueue.find({}).sort({ queue_score: -1 })
        res.send(queue)
    } catch (e) {
        res.status(400).send(e)
    }
});

router.get('/queue/:id', async (req, res) => {
    const _id = req.params.id;
    try {
        const queue = await myQueue.findById(_id);
        res.send({ message: queue });
    } catch (e) {
        res.status(404).send({ message: e });
    }
})

router.delete('/queue/delete/:id', async (req, res) => {
    const _id = req.params.id;
    try {
        const queue = await myQueue.findByIdAndDelete(_id);
        res.send({ message: "delete " + queue.queue_name });
    } catch (e) {
        res.status(404).send({ message: e });
    }
})

router.patch('/queue/update/:id', async (req, res) => {
    const _id = req.params.id;
    try {
        await myQueue.findByIdAndUpdate(_id, { $set: req.body }, function (err, result) {
            if (err) {
                console.log(err);
            }
            console.log("RESULT: " + result);
            res.status(202).send({ message: 'Done' })
        })
    } catch (e) {
        res.status(400).send({ message: e })
    }
});

router.get('/queue/score/:score', async (req, res) => {
    const _score = req.params.score;
    try {
        const queue = await myQueue.find({ queue_score: _score })
        res.send(queue);
        console.log(queue)
    } catch (e) {
        res.status(400).send({ message: e })
    }
});


// function positioning(new_score) {
//     var arr = [];
//     var j;
//     arr = Object.keys(myQueue.find({})).map(db => db.queue_score)
//     if (arr.length != 0) {
//         for (j = 0; j < arr.length - 1; j++) {
//             if (arr[j] <= new_score) {
//                 arr.splice(j, 0, new_score);
//                 break;
//             }
//             else {
//                 arr.push(new_score);
//                 break;
//             }
//         }
//     }
//     else {
//         arr.push(new_score);
//     }
//     return arr;
// }

function difference_day(queue_date) {
    let date = new Date();
    let years = date.getFullYear();
    let months = date.getMonth() + 1;
    let dates = date.getDate();

    let dayOfMonth = { 1: 31, 2: 28, 3: 31, 4: 30, 5: 31, 6: 30, 7: 31, 8: 31, 9: 30, 10: 31, 11: 30, 12: 31 };
    let queue_year = Number(queue_date[0] + queue_date[1] + queue_date[2] + queue_date[3]);
    let queue_month = Number(queue_date[5] + queue_date[6]);
    let queue_mydate = Number(queue_date[8] + queue_date[9]);

    let diff_year = years - queue_year;
    let diff_month = months - queue_month;
    let diff_date = dates - queue_mydate;

    var days = 0;

    days += diff_year * 365;

    var lis = [];
    let diff = queue_month + diff_month;
    for (i = queue_month; i < diff; i++) {
        if (i > 12) {
            i -= 12;
        }
        lis.push(i);
    }
    for (i in lis) {
        days += dayOfMonth[lis[i]];
    }
    days += diff_date;
    return days;
}

//Function การให้คะแนน
function sumScore(queue_create, queue_date, queue_priority) {
    //ให้คะแนน
    var score = 0;

    //เช็คจาก ความสำคัญ ความสำคัญจะ * 20 คะแนน
    score += queue_priority * 20;


    //เช็คคะแนนจาก ผู้ส่งคิว
    if (queue_create == 'Customer') {
        score += 10;
    }
    if (queue_create == 'Excutive') {
        score += 8;
    }
    if (queue_create == 'Insider') {
        score += 5;
    }
    if (queue_create == 'Outsider') {
        score += 3;
    }

    //คำนวนจำนวนวันที่แตกต่างกัน
    diff_day = difference_day(queue_date);
    //วันละ 4 คะแนน
    score += (4 * diff_day);
    return score;
}

//Function ในการกำหนด Priority ใหม่ โดยกำหนด จาก Score
function checkPriority(score) {
    //กำหนด priority ใหม่โดยคิดจาก คะแนน
    let priority = 0;
    if (score >= 0 && score <= 40) {
        priority = 1;
    }

    if (score >= 41 && score <= 80) {
        priority = 2;
    }

    if (score >= 81 && score <= 120) {
        priority = 3;
    }

    if (score >= 121 && score <= 160) {
        priority = 4;
    }

    if (score >= 160) {
        priority = 5;
    }
    return priority;
}

function validateForm(queue_name, queue_date, queue_about, queue_create, queue_priority) {

    if (typeof queue_name != "string" || queue_name == null) {
        console.log('name error');
        throw new Error('queue_name must be string');
    };
    if (!moment(queue_date, "YYYY-MM-DD LT", true).isValid() || queue_date == null) {
        console.log('date error');
        throw new Error('Invalid started! ex. 2014-12-13 12:34 PM');
    };
    if (typeof queue_about != "string" || queue_about == null) {
        console.log('about error');
        throw new Error('queue_about must be string');
    };

    var role = ["Excutive", "Customer", "Insider", "Outsider"]
    if (!role.includes(queue_create)) {
        console.log('create error');
        throw new Error('Must create by Excutive, Customer, Insider, Outsider')
    };

    if (queue_priority < 1 || queue_priority > 5) {
        console.log('priority error');
        throw new Error('Priority must be 1-5');
    };
}

module.exports = router;