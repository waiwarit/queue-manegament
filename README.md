# Queue Management
  ระบบการจัดการคิว จะทำการเรียงลำดับความสำคัญของงานต่างๆที่เข้า โดยจะให้เป็นคะแนน แล้วจะกำหนดความสำคัญให้กับงานนั้นๆ
## Queue Management Architure 
![Architure](images/architecture.png)

## Features  
  - #### การคิดคะแนน  
      1. ค่า priority * 20
      2. ดูจากผู้สร้างคิวประกอบไปด้วย 4 ลำดับ ได้แต่ Customer, Excutive, Insider, Outsider  
      โดย  Customer จะได้ 10 คะแนน  
      Excutive จะได้ 8 คะแนน  
      Insider จะได้ 5 คะแนน  
      Outsider จะได้ 3 คะแนน  
      3. คำนวนจากความต่างของวัน โดยจะให้ วันที่ค้าง เป็นวันละ 4 คะแนน เช่น สั่งงานมาแล้ว 2 วัน งานจะมีคะแนนเพิ่มขึ้นเป็น 8  
  - #### มีการกำหนด Priority ใหม่ โดย  
  Priority ระดับ 1 คะแนนจะต้องอยู่ในช่วง 0 - 40  
  Priority ระดับ 2 คะแนนจะต้องอยู่ในช่วง 41 -80  
  Priority ระดับ 3 คะแนนจะต้องอยู่ในช่วง 81 - 120  
  Priority ระดับ 4 คะแนนจะต้องอยู่ในช่วง 121- 160  
  Priority ระดับ 5 คะแนนจะต้องอยู่ในช่วง 161 ขึ้นไป  
  ---- 
## Usage :
  **** URL : `localhost:8080/sentwork  `  ****  
  **Method : `POST  `  **  
  **Example Request :  **  
```json
      {  
        "queue_date" : "2020-01-11 12:34 PM",  
        "queue_name" : "ประชุมหัวข้อ",  
        "queue_about" : "สรุปยอดขายประจำปี",  
        "queue_create" : "Excutive",  
        "queue_priority" : 4  
      }   
```
  Example Respon :  
```json
      {  
        "queue_date" : "2020-01-11 12:34 PM",  
        "queue_name" : "ประชุมหัวข้อ",  
        "queue_about" : "สรุปยอดขายประจำปี",  
        "queue_create" : "Excutive",  
        "queue_priority" : 4  
      }   
```
  ---
  **** URL : `locolhost:8080/queue  ` ****  
  **Method : `GET`  
  **Example Respon : **  
```json
    [
      {
        "_id": "5e2699f52d45fd0fb2d59ffd",
        "queue_date": "2020-01-11 12:34 PM",
        "queue_about": "สรุปยอดขายประจำปี",
        "queue_name": "Test01",
        "queue_priority": 5,
        "queue_score": 192,
        "queue_position": 0,
        "__v": 0
      },
      {
        "_id": "5e26e1ca7ae08d13fb28ab83",
        "queue_date": "2020-01-11 12:34 PM",
        "queue_about": "สรุปยอดขายประจำปี",
        "queue_create": "Insider",
        "queue_name": "Test01",
        "queue_priority": 5,
        "queue_score": 205,
        "queue_position": 0,
        "__v": 0
      }
    ]
```
  ---
  **** URL : `localhost:8080/queue/id` ****  
  **Method : `GET`  
  **Example Response :  **   
```json
      {
        "_id": "5e26e1ca7ae08d13fb28ab83",
        "queue_date": "2020-01-11 12:34 PM",
        "queue_about": "สรุปยอดขายประจำปี",
        "queue_create": "Insider",
        "queue_name": "Test01",
        "queue_priority": 5,
        "queue_score": 205,
        "queue_position": 0,
        "__v": 0
      }
```
  ---
  **** URL : `localhost:8080/queue/delete/id` ****  
  **Method : `DELETE`  **  
  **Example Response : **  
```json
      {
          "message": "delete Test03"
      }
```
  ---
  **** URL : `localhost:8080/queue/update/id` ****  
  **Method : `PATCH` **  
  **Example Response : **  
```json
      {
          "message": "update ID : 5e26f6f877d11a175d8f93a3 sucess"
      }
```
  


